LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libogg
LOCAL_CFLAGS += -I$(LOCAL_PATH)/../include -ffast-math -fsigned-char
ifeq ($(TARGET_ARCH),arm)
	LOCAL_CFLAGS += -marm -mfloat-abi=softfp -mfpu=vfpv3
endif


LOCAL_SRC_FILES := \
	bitwise.c \
	framing.c

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := OggTest
LOCAL_STATIC_LIBRARIES := libogg
include $(BUILD_SHARED_LIBRARY)
