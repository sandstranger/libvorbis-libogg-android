LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libvorbis
LOCAL_CFLAGS += -I$(LOCAL_PATH)/../include -ffast-math -fsigned-char
ifeq ($(TARGET_ARCH),arm)
	LOCAL_CFLAGS += -marm -mfloat-abi=softfp -mfpu=vfpv3
endif

LOCAL_SRC_FILES := \
	mdct.c		\
	smallft.c	\
	block.c		\
	envelope.c	\
	window.c	\
	lsp.c		\
	lpc.c		\
	analysis.c	\
	synthesis.c	\
	psy.c		\
	info.c		\
	floor1.c	\
	floor0.c	\
	res0.c		\
	mapping0.c	\
	registry.c	\
	codebook.c	\
	sharedbook.c	\
	lookup.c	\
	bitrate.c	\
	vorbisfile.c	\
	vorbisenc.c

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := VorbisTest
LOCAL_STATIC_LIBRARIES := libvorbis
include $(BUILD_SHARED_LIBRARY)
